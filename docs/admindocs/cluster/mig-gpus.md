## Partitioning GPUs (MIG)

1- Drain the node

2- Run `sudo nvidia-smi -mig 1` to convert **ALL** GPUs to MIGs.

3- Install `nvidia-mig-parted` on the node.

4- Istall `nvidia-mig-manager` on the node (https://github.com/NVIDIA/mig-parted/tree/main/deployments/systemd).

4- Write a config file `config.yaml` for the node to **/etc/nvidia-mig-manager/config.yam**.

An example is:

```yaml
version: v1
mig-configs:
  current:
  - devices: all
    mig-enabled: true
    mig-devices:
      1g.10gb: 7
  all-disabled:
  - devices: all
    mig-enabled: true
    mig-devices:
      1g.10gb: 7
```

You can also ssh into an exisiting MIG node and run `sudo nvidia-mig-parted export`.

5- Test persistence with reboots.

6- Don't forget to **uncordon**.