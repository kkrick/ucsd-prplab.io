## Cluster usage by node and namespace

!!! note "Additional Features available directly on website"

    Additional features, such as a downloadable table, can be viewed on the <a href="https://observablehq.com/@nrp-nautilus/node-allocated-resources-by-namespace">ObservableHQ Notebook.</a>

This is the experimental page for querying the monitoring information for nodes usage. Still work in progress. Data gathered since 09-15-23. Additional features, such as a downloadable table, can be viewed <a href="https://observablehq.com/@nrp-nautilus/node-allocated-resources-by-namespace">here</a>.
<div id="observablehq-statusMessage-936de095"></div>
<div id="observablehq-viewof-form-936de095"></div>
<div id="observablehq-viewof-nodeName-936de095"></div>
<div id="observablehq-viewof-zone-936de095"></div>
<div id="observablehq-viewof-region-936de095"></div>
<div id="observablehq-viewof-gpuChart-936de095"></div>
<div id="observablehq-viewof-cpuChart-936de095"></div>
<div id="observablehq-viewof-memoryChart-936de095"></div>
<div id="observablehq-viewof-storageChart-936de095"></div>
<div id="observablehq-viewof-fpgaChart-936de095"></div>
<p>Credit: <a href="https://observablehq.com/@nrp-nautilus/node-allocated-resources-by-namespace">Node Allocated Resources (By Namespace) by NRP Nautilus</a></p>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@observablehq/inspector@5/dist/inspector.css">
<script type="module">
import {Runtime, Inspector} from "https://cdn.jsdelivr.net/npm/@observablehq/runtime@5/dist/runtime.js";
import define from "https://api.observablehq.com/@nrp-nautilus/node-allocated-resources-by-namespace.js?v=4";
new Runtime().module(define, name => {
  if (name === "statusMessage") return new Inspector(document.querySelector("#observablehq-statusMessage-936de095"));
  if (name === "viewof form") return new Inspector(document.querySelector("#observablehq-viewof-form-936de095"));
  if (name === "viewof nodeName") return new Inspector(document.querySelector("#observablehq-viewof-nodeName-936de095"));
  if (name === "viewof zone") return new Inspector(document.querySelector("#observablehq-viewof-zone-936de095"));
  if (name === "viewof region") return new Inspector(document.querySelector("#observablehq-viewof-region-936de095"));
  if (name === "viewof gpuChart") return new Inspector(document.querySelector("#observablehq-viewof-gpuChart-936de095"));
  if (name === "viewof cpuChart") return new Inspector(document.querySelector("#observablehq-viewof-cpuChart-936de095"));
  if (name === "viewof memoryChart") return new Inspector(document.querySelector("#observablehq-viewof-memoryChart-936de095"));
  if (name === "viewof storageChart") return new Inspector(document.querySelector("#observablehq-viewof-storageChart-936de095"));
  if (name === "viewof fpgaChart") return new Inspector(document.querySelector("#observablehq-viewof-fpgaChart-936de095"));
  return ["results","data","datatable"].includes(name);
});
</script>
